import pytest
from selenium.webdriver import ActionChains

@pytest.fixture(name="base_url", scope="session")
def base_url_fixture():
    return "http://joyreactor.cc"


def login_to_vote_test(driver, base_url):
    driver.get(base_url)
    vote_plus = driver.find_element_by_class_name('vote-plus')

    (
        ActionChains(driver)
        .move_to_element(vote_plus)
        .click(vote_plus)
        .perform()
    )

    auth_popup = driver.find_element_by_class_name("SignInOrSignUpDialog")
    assert auth_popup is not None

